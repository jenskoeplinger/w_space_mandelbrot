// ======================================================================
// W space: Multi star - fractal graph creation tool
// (CC) 2010 Jens Koeplinger, http://www.jenskoeplinger.com/P
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspacemultistarfractal.c" for what this means to you!
// ======================================================================
// help-wspacemultistarfractal.c
// ----------------------------------------------------------------------
// prints help to stdout

    printf("This program calculates the 'Multi-star', a fractal shape\n");
    printf("in 'W space'. Beginning at the point (a, b) at the respective\n");
    printf("position in the output bitmap, this value is iteratively\n");
    printf("squared, up to a certain cut-off condition. Since the result\n");
    printf("of a multiplication in W space may in general have two\n");
    printf("possible values, the number of possible results from squaring\n");
    printf("the initial point would be 2^N, after N iterations.\n");
    printf("In the approximation for N against infinity, a certain\n");
    printf("percentage of results would approach infinity, whereas the\n");
    printf("remainder would approach 0 (or remain finite). The 'Multi-star'\n");
    printf("then is the fractal that displays this convergence / divergence\n");
    printf("percentage, e.g. through shades of gray (black=full convergence,\n");
    printf("white=full divergence), or color gradients, or alternating color\n");
    printf("pallettes. The advantage of a smooth gradient in the chosen\n");
    printf("color pallette is identification of percentage plateaus in the\n");
    printf("resulting fractal. The advantage of alternating color pallettes\n");
    printf("is in highlighting the structure of the fractal.\n");
    printf("\n");
    printf("It is (optionally) possible to perform a Mandelbrot-like\n");
    printf("algorithm. When chosing to do so, the value of the\n");
    printf("beginning point (a, b) is added to the iteration value after\n");
    printf("each squaring, just as in the Mandelbrot set in the complex\n");
    printf("numbers.\n");
    printf("\n");
    printf("Command-line parameters are:\n");
    printf("  [1] threshold depth: iteration cutoff depth (default: 10)\n");
    printf("  [2] real left coordinate of the bitmap (default: -2.2)\n");
    printf("  [3] real right coordinate (default: 2.2)\n");
    printf("  [4] w bottom (default: -2.2)\n");
    printf("  [5] w top (default: 2.2)\n");
    printf("  [6] bitmap size in pixels (default: 800)\n");
    printf("  [7] display unit circle [0 or 1] (default: 0 for no display)\n");
    printf("  [8] color template (default: 2 for 766 continuous shades of gray)\n");
    printf("  [9] perform Mandelbrot algorithm [0 or 1] (default: 0 for no)\n");
    printf("\n");
    printf("Example:\n");
    printf("  ./a.out 20 -2 2 -2 2 1000 0 2 0\n");
    printf("This example renders up to iteration depth 20, in the [-2,2] real\n");
    printf("interval and [-2, 2] w interval, produces a bitmap of size 1000x1000,\n");
    printf("does not display the unit circle, and uses color template 2 (which are\n");
    printf("766 shades of gray).\n");
    printf("\n");
    printf("A histogram file will be prepared for each color, with the\n");
    printf("following column values (filename is 'wspace.csv'):\n");
    printf("  - color number\n");
    printf("  - number of pixels with this color\n");
    printf("  - average value for this pixel\n");
    printf("  - a 'pseudo standard deviation' parameter (non-quantitative)\n");
    printf("  - number of pixels within unit circle\n");
    printf("  - number of pixels within both (w) and (-w) ellpises\n");
    printf("  - number of pixels within (w) ellipse\n");
    printf("A color with a low 'pseudo standard deviation' indicates a\n");
    printf("plateau. A color with a higher value indicates\n");
    printf("a more complex region to be mapped by this color.\n");

    printRecursiveSquaringColorTemplateInfo();
