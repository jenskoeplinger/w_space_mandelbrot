// ======================================================================
// W space: Multi star - fractal graph creation tool
// (CC) 2010 Jens Koeplinger, http://www.jenskoeplinger.com/P
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspacemultistarfractal.c" for what this means to you!
// ======================================================================
// lib-multistar-calculate.h
// ----------------------------------------------------------------------
// run convergent / divergent iterative self-squaring w. branching;
// generates an output bitmap and/or histogram CSV file.
//
// Example:
/*

    convergentSelfSquaringWithBranching();
       (...)
    thisPercent = squareRecursive(pix_real, pix_w, 0, thresholdDepth);

*/
// ---------------------------------------------------------------------- 

// declare the following as global so that they won't need to be handed
// down with every iteration:
double pix_real;              // real coordinate of current point
double pix_w;                 // ... w coordinate

void convergentSelfSquaringWithBranching() {
//  This function calculates an approximation of the following
//  algorithm: For each point in the (1, w) plane, the two squares of this
//  point would be calculated, one value for "squaring" along each of the
//  power orbits (w) and (-w). These two points become two "branches".
//  Then, these two points ("branches") are squared again, yielding 4
//  results (i.e. 2 new branches each). Then, these 4 results are
//  squared again, yielding 8, and so on. The number of points grows
//  exponentiall with 2^n where  n is the number of iteration steps.
//  For a ficticuous infinite amount of iterations, the list of points
//  would either be convergent (i.e. norm --> 0) or divergent
//  (i.e. norm --> infy). The percentage (between 0% and 100% could
//  be plotted in a height-graph).
//
//  Executing this calculation iteratively is very computation time
//  intensive, and can be simplified due to the following rules:
//
//  Rule 1) If the smaller of the norms is greater than sqrt(3), then
//          all points in the iteration must be divergent. This is
//          because the difference in the norms between a point and
//          its copoint is at most sqrt(3); with a multiplicative
//          norm, the square of such a point would be greater than
//          sqrt(3)^2 = 9, and transition to the smaller copoint norm
//          would in the worst case give a point with norm greater
//          than sqrt(3). Since sqrt(3+e)^2 = 9 + O(6e) this iteration
//          is divergent and will go against infinity.
//  Rule 2) Similar to rule 1, if the larger of the norms of a
//          point is smaller than 1/sqrt(3), then all subsequently
//          squared points must be convergent.
//
//  When requesting Mandelbrot-style calculation (makeMandel == 1),
//  the cutoff-rules are simplified, to abort iteration when the norm
//  is greater than 50.
//
//  Using these rules prunes the iteration tree, and eventually leads
//  for a cut-off condition in most cases. For the remainder, a hard
//  exit criterion is specified by the depth of the iteration tree.

    int    numColors      = 16;   // number of colors in the pallette for the
                                  // current output bitmap color template

//  iteration runtime parameters:
    int    isUnitCircle   = 0;    // whether the current point is within the unit circle
    int    this_depth     = 0;    // current point's iteration depth
    double thisPercent    = -1.;  // current convergence percentage (0..1)

    unsigned long int unitCircleCnt;
                                  // total number of pixels in the unit circle

//  other:
    int    runAgain       = 1;
    FILE   *fp;                   // file pointer for wspace.xpm bitmap file
    FILE   *fpHist;               // file pointer for histogram file wspace.csv

//  histogram file parameters
    unsigned long int histCount[maxNumColors];           // pixel count for this color
    unsigned long int histUnitCircleCount[maxNumColors]; // number of pixels in unit circle
    unsigned long int histTwoEllipseCount[maxNumColors]; // number of pixels within both unit ellipses
    unsigned long int histOneEllipseCount[maxNumColors]; // number of pixels in (w) unit ellipse
    double histAvg[maxNumColors];                        // average value for this color
    double histStdDev[maxNumColors];                     // an std-dev-like parameter thereof
    double thisStdDev;                                   // throwaway

    double n_real1;
    double n_real2;
    double n_w1;
    double n_w2;
    double thisColorD;
    int    thisColor;
    int    num_columns;                                   // = outFileSize
    int    num_rows;                                      // = outFileSize
    int    this_row;
    int    this_column;

    printf("Maximum recursion depth: %d\n", thresholdDepth);
    printf("Real axis, left parameter bound: %f\n", real_left);
    printf("Real axis, right parameter bound: %f\n", real_right);
    printf("W axis, lower parameter bound: %f\n", w_bottom);
    printf("W axis, upper parameter bound: %f\n", w_top);
    printf("Output type: Bitmap (XPM)\n");
    printf("Output file 'wspace.xpm' is %d x %d pixels\n", outFileSize, outFileSize);
    printf("Color template: %d\n", colorTemplate);
    printf("Perform Mandelbrot-style calc: %d\n\n", makeMandel);

    num_columns = outFileSize;
    num_rows    = outFileSize;

    fp = fopen("wspace.xpm", "wb");
    if (fp == NULL) {
        printf("PANIC - file 'wspace.xpm' didn't open\n\n**-BP4-**\n");
        return;
    }

    // print file header (type, size, pallette)
    fprintf(fp, "! XPM2\n");
    fprintf(fp, "%d %d", outFileSize, outFileSize + 10);
    numColors = writeColorTemplateToFile(fp, colorTemplate);

    // write the color pallette into the first 10 pixels
    this_row = 10;

    while (this_row-- > 0) {

        this_column = num_columns;

        while (this_column-- > 0) {

            if (this_column < numColors)
                fprintf(fp, "%04X", this_column);
            else
                fprintf(fp, "%04X", (numColors - 1) * (this_row % 2));
        }

        fprintf(fp, "\n");

    }

    // reset histogram
    thisColor = maxNumColors;
    while (thisColor-- > 0) {

        histCount[thisColor]           = 0;
        histAvg[thisColor]             = 0;
        histStdDev[thisColor]          = 0;
        histUnitCircleCount[thisColor] = 0;
        histTwoEllipseCount[thisColor] = 0;
        histOneEllipseCount[thisColor] = 0;

    }

    // execute the iteration
    // this_row = 0 .. num_rows-1
    // this_column = 0 .. num_columns-1

    this_row             = 0;
    iterationCounterLow  = 0;
    iterationCounterHigh = 0;
    unitCircleCnt        = 0;

    while ((numColors > 0) && (this_row < num_rows)) do {

        this_column = 0;

        do {
            // compile the iteration start values

            this_depth = 0;

            pix_real = real_left
                       + ((double) this_column)
                         * (real_right - real_left)
                         / ((double) (num_columns - 1));

            pix_w = w_top
                       + ((double) this_row)
                         * (w_bottom - w_top)
                         / ((double) (num_rows - 1));

            if ((pix_real * pix_real + pix_w * pix_w) < 1.) {

                isUnitCircle = 1;
                unitCircleCnt++;

            } else {

                isUnitCircle = 0;

            }

            // ==================================
            // RECURSION BEGINS HERE
            thisPercent = squareRecursive(pix_real, pix_w, 0, thresholdDepth);
            // RECURSION ENDS HERE
            // ==================================

            // thisPercent is a value from 0. through 1.; translate
            // this to one of the numColors "colors"; do a cut-off in a way
            // that only 1. truly gets value black, and only 0. gets
            // value white, as much as precision allows

            thisColorD = (((double) numColors) - 2.)
                         - (thisPercent * (((double) numColors) - 2.));

            if (thisColorD < 1E-20) {
                thisColor = 0;
            } else if (thisColorD > (((double) numColors) - 2.000000000000001)) {
                thisColor = numColors - 1;
            } else {
                thisColor = (int) (thisColorD + 1.);
            }

            // print to file

            if ((unitCircle == 1) && (isUnitCircle == 1))
                fprintf(fp, "%04X", (thisColor + (numColors / 2)) % numColors);
            else
                fprintf(fp, "%04X", thisColor);

            // record histogram arrays

            if (histCount[thisColor] == 0) {
                // this is the first point of this color

                histCount[thisColor]  = 1;
                histAvg[thisColor]    = thisPercent;
                histStdDev[thisColor] = 0.;

            } else {
                // at least one other point exists; build average
                // and stdDev-like parameter

                histCount[thisColor]  = histCount[thisColor] + 1;
                histStdDev[thisColor] = histStdDev[thisColor]
                                        + (histAvg[thisColor] - thisPercent)
                                            * (histAvg[thisColor] - thisPercent);
                histAvg[thisColor]    = histAvg[thisColor]
                                            * (1. - 1. / ((double) histCount[thisColor]))
                                        + thisPercent
                                            * (1. / ((double) histCount[thisColor]));

            }

            if (isUnitCircle)
                histUnitCircleCount[thisColor] = histUnitCircleCount[thisColor] + 1;

            if ((pix_real * pix_real - pix_real * pix_w + pix_w * pix_w) < 1.) {
                // point is within the (w) unit ellipse (norm = 1)

                histOneEllipseCount[thisColor] = histOneEllipseCount[thisColor] + 1;

                if ((pix_real * pix_real + pix_real * pix_w + pix_w * pix_w) < 1.) {
                    // point is also within (-w) unit ellipse

                    histTwoEllipseCount[thisColor] = histTwoEllipseCount[thisColor] + 1;

                }

            }

        } while ((++this_column) < num_columns);

        // print new line to file and status update to screen

        fprintf(fp, "\n");

        if ((this_row + 1 == num_rows) || (this_row % 50) == 0) {
            printf("\nFinished row %d of %d (recursions: ",
                       this_row + 1, num_rows);
            printRecursionCounter(0);
            printf(")\n");

        } else {

            printf(".");

        }

    } while ((++this_row) < num_rows);

    // done - output written
    // close the file

    fclose(fp);

    // iteration complete

    printf("Bitmap complete\n");
    printf("  - pixels: %lu\n",
                ((unsigned long int) this_column)
                    * ((unsigned long int) this_row));
    printf("  - pixels in unit circle: %lu\n",
                unitCircleCnt);
    printf("  - # recursive calculations: ");
    printRecursionCounter(0);
    printf("\n\n");

    // write histogram file

    fpHist = fopen("wspace.csv", "wb");

    if (fpHist == NULL) {
        // too bad it didn't open, but no reason to PANIC

        printf("ERROR - histogram file 'wspace.csv' didn't open\n");
        printf("  --- too bad --- ignore and continue\n\n");

    } else {
        // color histogram file ready for writing

        fprintf(fpHist, "ColorNumber,");
        fprintf(fpHist, "PixelCount,");
        fprintf(fpHist, "AveragePercentage,");
        fprintf(fpHist, "ApproximateStandardDeviation,");
        fprintf(fpHist, "PixelInUnitCircle,");
        fprintf(fpHist, "PixelInWEllipse,");
        fprintf(fpHist, "PixelInBothEllipses\n");

        thisColor = numColors;

        while (thisColor-- > 0) {
            // write histogram, beginning with the divergent color
            // (i.e. with the highest index first)

            // for each color, normalize the near-stdDev
            // value by the number of points less one
            if (histCount[thisColor] > 1) {

                histStdDev[thisColor] = sqrt(histStdDev[thisColor])
                      / ((double) (histCount[thisColor] - 1));

            }

            fprintf(fpHist, "%d,",     thisColor);
            fprintf(fpHist, "%lu,",    histCount[thisColor]);
            fprintf(fpHist, "%.15e,",  histAvg[thisColor]);
            fprintf(fpHist, "%.15e,",  histStdDev[thisColor]);
            fprintf(fpHist, "%lu,",    histUnitCircleCount[thisColor]);
            fprintf(fpHist, "%lu,",    histOneEllipseCount[thisColor]);
            fprintf(fpHist, "%lu\n",   histTwoEllipseCount[thisColor]);

        }

        // all done - close file
        fclose(fpHist);
        printf("Histogram file 'wspace.csv' written OK\n\n");

    }

}

// ----------------------------------------------------------------------
// (12.2) perform the recursion
// ----------------------------------------------------------------------

double squareRecursive(double inReal, double inW, int inDepth, int maxDepth) {
// IN:  (inReal, inW) is the point to be squared
//      inDepth is the current recursion depth
//      maxDepth is the maximum depth
// OUT: average convergent points, [ 0. , 1. ]

    double outReal;
    double outWPlus;
    double outWMinus;
    double normPlusPlus;   // the larger of the norms on the (w) orbit result
    double normPlusMinus;  // the greater of the norms on the (w) orbit result
    double normMinusPlus;  // the greater of the norms on the (-w) orbit result
    double normMinusMinus; // the lesser of the norms on the (-w) orbit result
    double convPlus;       // 1 if (w) orbit point is convergent, 0 if divergent, 0.5 if undecided
    double convMinus;      // same for (-w) orbit point

    printRecursionCounter(1);

    outReal   = inReal * inReal - inW * inW;
    outWPlus  = (2. * inReal + inW) * inW;
    outWMinus = (2. * inReal - inW) * inW;
    
    if (makeMandel == 1) {
    // Mandelbrot-style calculation requested; add current pixel point value
    
        outReal   = outReal   + pix_real;
	outWPlus  = outWPlus  + pix_w;
	outWMinus = outWMinus + pix_w;
    
    }

    normPlusPlus   = sqrt(outReal * outReal + outReal * outWPlus  + outWPlus  * outWPlus);
    normPlusMinus  = sqrt(outReal * outReal - outReal * outWPlus  + outWPlus  * outWPlus);
    normMinusPlus  = sqrt(outReal * outReal + outReal * outWMinus + outWMinus * outWMinus);
    normMinusMinus = sqrt(outReal * outReal - outReal * outWMinus + outWMinus * outWMinus);

    if (inDepth == maxDepth) {
        // maximum recursion depth reached;
        // check each of the four norms and build a rough
        // estimate on behavior for infinity

        if (makeMandel == 1) { return 1.; } // Mandelbrot-style iteration assumes convergent only when iteration depth reached

        convPlus = 0.;
	
        if (normPlusPlus   < 1.0) convPlus = convPlus + 0.25;
        if (normPlusMinus  < 1.0) convPlus = convPlus + 0.25;
        if (normMinusPlus  < 1.0) convPlus = convPlus + 0.25;
        if (normMinusMinus < 1.0) convPlus = convPlus + 0.25;

        return convPlus;

    } else {
        // maximum recursion depth not yet reached;
        // check each point

        if (makeMandel == 1) {
            // Mandelbrot algorithm requested; abort if norm > 50.
	
	    if ((normMinusMinus + normMinusPlus + normPlusMinus + normPlusPlus) > 200.) {
	        // norm too large: consider divergent
	      
	        return 0.;
		
	    } else {
	        // do a deeper Mandelbrot iteration (and return)
	    
	        convPlus  = squareRecursive(outReal, outWPlus,  inDepth + 1, maxDepth);
		convMinus = squareRecursive(outReal, outWMinus, inDepth + 1, maxDepth);
		
	        return (convPlus + convMinus) / 2.;
	    
	    }
	    
	}

        // 1) check (w) point

        if (normPlusMinus > 3.) {
            // must be divergent for all subsequent recursions; abort

            convPlus = 0.;

        } else if (normPlusPlus < 0.333333333333) {
            // must be convergent for all subsequent recursions; abort

            convPlus = 1.;

        } else {
            // divergence / convergence to be determined one level deeper

            convPlus = squareRecursive(outReal, outWPlus, inDepth + 1, maxDepth);

        }

        // 2) check (-w) point

        if (normMinusMinus > 3.) {
            // must be divergent for all subsequent recursions; abort

            convMinus = 0.;

        } else if (normMinusPlus < 0.333333333333) {
            // must be convergent for all subsequent recursions; abort

            convMinus = 1.;

        } else {
            // divergence / convergence to be determined one level deeper

            convMinus = squareRecursive(outReal, outWMinus, inDepth + 1, maxDepth);

        }

        // ... and return the average

        return (convPlus + convMinus) / 2.;

    }

}

