// ===========================================================================
// W space: Multi star - fractal graph creation tool
// (CC) 2010 Jens Koeplinger
// http://www.jenskoeplinger.com/W
//
// Reference: J. A. Shuster and J. Koeplinger,
// "Elliptic complex numbers with dual multiplication."
// Appl. Math. Comput. 216 (2010), pp. 3497-3514; doi: 10.1016/j.amc.2010.04.069
// http://www.jenskoeplinger.com/P/PaperShusterKoepl_WSpace.pdf
//
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
//
// You are free:
// - to Share - to copy, distribute and transmit the work
// - to Remix - to adapt the work
//
// Under the following conditions:
// - Attribution. You must attribute the work in the manner specified by the
//   author or licensor (but not in any way that suggests that they endorse
//   you or your use of the work).
// - Share Alike. If you alter, transform, or build upon this work, you may
//   distribute the resulting work only under the same, similar or a compatible
//   license.
//
// For any reuse or distribution, you must make clear to others the license
// terms of this work. The best way to do this is with a link to:
//   http://creativecommons.org/licenses/by-sa/3.0
//
// Any of the above conditions can be waived if you get permission from the
// copyright holder.
//
// Nothing in this license impairs or restricts the author's moral rights.
// ===========================================================================

#define WspaceToolVersion "W space - Multi star fractal, version 0.7.0 (25 July 2010)\n"
#define licenseMain       "(CC) 2010 Jens Koeplinger - http://www.jenskoeplinger.com/\n"
#define licenseDetail     "License: Creative Commons Attribution - Share Alike 3.0 Unported\n"
#define licenseReference  "http://creativecommons.org/licenses/by-sa/3.0\n\n"

#define sqrt3             1.732050807568877
#define invSqrt3          0.5773502691896259
#define numColorTemplates 9
#define maxNumColors      20000
#define piD               3.141592653589793

#include <stdlib.h>   // random functions
#include <stdio.h>
#include <math.h>

// ---------------------------------------------------------------------------
// global declarations - functions
// ---------------------------------------------------------------------------

// lib-multistar-calculate.h
void   convergentSelfMultRunText(int, int);
void   convergentSelfSquaringWithBranching();
double squareRecursive(double, double, int, int);
void   convergentSelfMultiplicationWithBranching();
double multiplyRecursiveWithOriginalPoint(double, double, double, double, int, int);

// lib-multistar-XPM.h
int    writeColorTemplateToFile(FILE *, int);
void   printRecursiveSquaringColorTemplateInfo(void);
void   printRecursionCounter(int);

// ---------------------------------------------------------------------------
// global declarations - variables
// ---------------------------------------------------------------------------

unsigned long int iterationCounterLow;
unsigned long int iterationCounterHigh;

int    thresholdDepth =  10  ;   // recursion depth cut-off
double real_left      =  -2.2;   // left real parameter bound of the output bitmap
double real_right     =   2.2;
double w_bottom       =  -2.2;   // lower (bottom) w parameter bound
double w_top          =   2.2;
int    outFileSize    = 800  ;   // pixels width and height of output bitmap
int    unitCircle     =   0  ;   // show unit circle: 1, otherwise: 0
int    colorTemplate  =   0  ;   // see writeColorTemplateToFile(FILE, int) for more info
int    makeMandel     =   0  ;   // whether to calculate Mandelbrot-style algorithm (1) or not (0)

// ---------------------------------------------------------------------------
// main()
// ---------------------------------------------------------------------------

int main(int argc, char **argv) {

    int i; // throwaway

    printf(WspaceToolVersion);
    printf(licenseMain);
    printf(licenseDetail);
    printf(licenseReference);
    printf("For HELP, run without command-line parameters.\n");
    printf("\nFor documentation on the multi-star fractal, see:\n");
    printf("   APPENDIX A, in\n");
    printf("   J. A. Shuster and J. Koeplinger,\n");
    printf("   \"Elliptic complex numbers with dual multiplication.\n");
    printf("   Appl. Math. Comput. 216 (2010), pp. 3497-3514;\n");
    printf("   doi: 10.1016/j.amc.2010.04.069.\n");
    printf("   http://www.jenskoeplinger.com/P/PaperShusterKoepl_WSpace.pdf\n\n");

    // read command line parameters
    if ((argc == 1) || (argv[1][0] == '-')) {
        //  no command line argument given,
        // or first argument started with dash;
        // --> print help and abort

#include "help-wspacemultistarfractal.c"

        return;

    }

    //  at least one command line argument given; display raw
    printf("[NOTE] For help: run without arguments\n");
    printf("-------------------------------------\n");
    printf("Number of command line arguments = %d\n", argc);

    for (i = 0; i < argc; i++) {
        printf("- Arg. #%d = \"%s\"\n", i, argv[i]);
    }

//  (naively) parse command line parameters, if present, and
//  convert them, and overwrite global variables

    if (argc > 1) thresholdDepth = atoi(argv[1]);
    if (argc > 2) real_left      = atof(argv[2]);
    if (argc > 3) real_right     = atof(argv[3]);
    if (argc > 4) w_bottom       = atof(argv[4]);
    if (argc > 5) w_top          = atof(argv[5]);
    if (argc > 6) outFileSize    = atoi(argv[6]);
    if (argc > 7) unitCircle     = atoi(argv[7]);
    if (argc > 8) colorTemplate  = atoi(argv[8]);
    if (argc > 9) makeMandel     = atoi(argv[9]);

    if (argc > 10) {
    //  ignore any additional arguments, but print a warning

        printf("[INFO] More than 9 arguments provided; ignoring the additional ones.\n");

    }

    convergentSelfSquaringWithBranching();

    printf("Good-bye\n");

}

// ---------------------------------------------------------------------------

#include "lib-multistar-calculate.h"
#include "lib-multistar-XPM.h"

// ---------------------------------------------------------------------------
