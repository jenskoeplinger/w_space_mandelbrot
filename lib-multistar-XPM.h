// ======================================================================
// W space: Multi star - fractal graph creation tool
// (CC) 2010 Jens Koeplinger, http://www.jenskoeplinger.com/P
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspacemultistarfractal.c" for what this means to you!
// ======================================================================
// lib-multistar-XPM.h
// ----------------------------------------------------------------------
// some support functions relating to graphics and XPM bitmap output.
//
// Example:
/*

    printRecursiveSquaringColorTemplateInfo();
    numColors = writeColorTemplateToFile(fp, colorTemplate);

*/

// ----------------------------------------------------------------------
// write the currently selected color template to file
// ----------------------------------------------------------------------

int writeColorTemplateToFile(FILE *fp, int colorTemplate) {
// IN:  *fp is a pointer to the current file that's open for writing
//      colorTemplate is the number of the color template
// OUT: number of colors in the selected color template
//
// This function writes the color pallette for the selected template
// to the XPM file, and returns the number of colors in this template.
//
// Description of color profiles
// -----------------------------
//
// 0:   16 grayshades, from white (divergent) to black (convergent)
// 1:   256 grayshades,          ="=
// 2:   766 grayshades,          ="=
// 3:   2039 color sweep,        ="=
// 4:   16*766 grayshades,       ="= (16 times repeated within the pallette)
// 5:   1 + 256*16 grayshades,   ="= aligned such that each 1/(2^n) falls on white
// 6:   1 + 4096 black-white alternate (hard contrast)
// 7:   1 + 4096 light green (divergent) to light red (convergent) gradient,
//      alternating with static blue (hard contrast)
// 8:   same as 3 but an additional alternating hard-contrast is added; 4077
//      colors total
// 9:   color spectrum that is similar to a prism, from red to violet

    int    colorCount = 0;
    int    colorCountO = 0; // support counter
    double ccD; // (double) colorCount;
    int    colorRed;
    int    colorGreen;
    int    colorBlue;

    switch (colorTemplate) {

        case 0: // 16 grayshades

            colorCount = 0;
            fprintf(fp, " 16 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%X%X%X%X%X%X\n",
                    colorCount * 16,
                    colorCount * 16,
                    colorCount * 16,
                    colorCount * 16,
                    colorCount * 16,
                    colorCount * 16);

            } while (++colorCount < 16);

            return 16;
            break;

        case 1: // 256 grayshades

            colorCount = 0;
            fprintf(fp, " 256 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%02X%02X%02X\n",
                    colorCount,
                    colorCount,
                    colorCount);

            } while (++colorCount < 256);

            return 256;
            break;

    case 2: // 766 grayshades

            colorCount = 0;
            fprintf(fp, " 766 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%02X%02X%02X\n",
                    (colorCount + 2) / 3,
                    (colorCount + 1) / 3,
                    colorCount / 3         );

            } while (++colorCount < 766);

            return 766;
            break;

        case 3: // 2039 color sweep

            colorCount = 0;
            fprintf(fp, " 2039 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");

                if (colorCount < 200) {

                    colorRed = 0;
                    colorGreen = 0;
                    colorBlue = colorCount;

                } else if (colorCount < 400) {

                    colorRed = colorCount - 200;
                    colorGreen = 0;
                    colorBlue = 300 - colorCount / 2;

                } else if (colorCount < 600) {

                    colorRed = 400 - colorCount / 2;
                    colorGreen = colorCount - 400;
                    colorBlue = 300 - colorCount / 2;

                } else if (colorCount < 800) {

                    colorRed = 100;
                    colorGreen = 500 - colorCount / 2;
                    colorBlue = colorCount - 600;

                } else if (colorCount < 1000) {

                    colorRed = colorCount / 2 - 300;
                    colorGreen = 100;
                    colorBlue = 1000 - colorCount;

                } else if (colorCount < 1156) {

                    colorRed = 200;
                    colorGreen = colorCount - 900;
                    colorBlue = 0;

                } else if (colorCount < 1215) {

                    colorRed = colorCount + 256 - 1215;
                    colorGreen = (256 + 200) - colorRed;
                    colorBlue = 0;

                } else if (colorCount < 1471) {

                    colorRed = 256;
                    colorGreen = ((1471 - colorCount) * 200) / 256;
                    colorBlue = colorCount + 256 - 1471;

                } else if (colorCount < 1727) {

                    colorRed = (1727 + 100) - colorCount;
                    colorGreen = colorCount + 256 - 1727;
                    colorBlue = 256;

                } else if (colorCount < 1883) {

                    colorRed = colorCount + 100 - 1727;
                    colorGreen = 256;
                    colorBlue = (1883 + 100) - colorCount;

                } else {

                    colorRed = 256;
                    colorGreen = 256;
                    colorBlue = colorCount + 100 - 1883;

                }

                if (colorRed   > 255) colorRed   = 255;
                if (colorRed   < 0  ) colorRed   = 0;
                if (colorGreen > 255) colorGreen = 255;
                if (colorGreen < 0  ) colorGreen = 0;
                if (colorBlue  > 255) colorBlue  = 255;
                if (colorBlue  < 0  ) colorBlue  = 0;

                fprintf(fp, "%02X%02X%02X\n",
                            colorRed,
                            colorGreen,
                            colorBlue         );

            } while (++colorCount < 2039);

            return 2039;
            break;

    case 4: // 16*766 = 12256 grayshades

            colorCount = 0;
            fprintf(fp, " 12256 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%02X%02X%02X\n",
                    ((colorCount % 766) + 2) / 3,
                    ((colorCount % 766) + 1) / 3,
                    (colorCount % 766) / 3        );

            } while (++colorCount < 12256);

            return 12256;
            break;

        case 5: // 1 + 256*16 grayshades = 4097

           colorCount = 0;
           fprintf(fp, " 4097 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%X%X%X%X%X%X\n",
                    (15 - colorCount % 16) * 16,
                    (15 - colorCount % 16) * 16,
                    (15 - colorCount % 16) * 16,
                    (15 - colorCount % 16) * 16,
                    (15 - colorCount % 16) * 16,
                    (15 - colorCount % 16) * 16
                    );

            } while (++colorCount < 4097);

            return 4097;
            break;

        case 6: // 1 + 4096 black-white alternate (hard contrast)

           colorCount = 0;
           fprintf(fp, " 4097 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%X%X%X%X%X%X\n",
                    (1 - colorCount % 2) * 255,
                    (1 - colorCount % 2) * 255,
                    (1 - colorCount % 2) * 255,
                    (1 - colorCount % 2) * 255,
                    (1 - colorCount % 2) * 255,
                    (1 - colorCount % 2) * 255
                    );

            } while (++colorCount < 4097);

            return 4097;
            break;

        case 7: // 1 + 4096 light-green (divergent) through light-red
                // (convergent) gradient, alternating over static
                // blue (hard contrast)

           colorCount = 0;
           fprintf(fp, " 4097 4\n");

            do {

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");

                if (colorCount % 2 == 1) {

                    fprintf(fp, "0000FF");

                } else {

                    colorGreen = (int) (255. * (((double) colorCount) / 4097.));
                    colorRed = (int) (255. - 255. * (((double) colorCount) / 4097.));

                }

                fprintf(fp, "%02X%02X%02X\n",
                        colorRed,
                        colorGreen,
                        0           );

            } while (++colorCount < 4097);

            return 4097;
            break;


        case 8: // 2039 color sweep plus alternating counter-contrast
                // (4077 colors total)

            colorCountO = 0;
            fprintf(fp, " 4077 4\n");

            do {

                fprintf(fp, "%04X c ", colorCountO);
                fprintf(fp, "#");

                if ((colorCountO % 2) == 0) {
                    // this is on a 1/(2^n) percentage; make colorful gradient

                    colorCount = colorCountO / 2;

                    if (colorCount < 200) {

                        colorRed = 0;
                        colorGreen = 0;
                        colorBlue = colorCount;

                    } else if (colorCount < 400) {

                        colorRed = colorCount - 200;
                        colorGreen = 0;
                        colorBlue = 300 - colorCount / 2;

                    } else if (colorCount < 600) {

                        colorRed = 400 - colorCount / 2;
                        colorGreen = colorCount - 400;
                        colorBlue = 300 - colorCount / 2;

                    } else if (colorCount < 800) {

                        colorRed = 100;
                        colorGreen = 500 - colorCount / 2;
                        colorBlue = colorCount - 600;

                    } else if (colorCount < 1000) {

                        colorRed = colorCount / 2 - 300;
                        colorGreen = 100;
                        colorBlue = 1000 - colorCount;

                    } else if (colorCount < 1156) {

                        colorRed = 200;
                        colorGreen = colorCount - 900;
                        colorBlue = 0;

                    } else if (colorCount < 1215) {

                        colorRed = colorCount + 256 - 1215;
                        colorGreen = (256 + 200) - colorRed;
                        colorBlue = 0;

                    } else if (colorCount < 1471) {

                        colorRed = 256;
                        colorGreen = ((1471 - colorCount) * 200) / 256;
                        colorBlue = colorCount + 256 - 1471;

                    } else if (colorCount < 1727) {

                        colorRed = (1727 + 100) - colorCount;
                        colorGreen = colorCount + 256 - 1727;
                        colorBlue = 256;

                    } else if (colorCount < 1883) {

                        colorRed = colorCount + 100 - 1727;
                        colorGreen = 256;
                        colorBlue = (1883 + 100) - colorCount;

                    } else {

                        colorRed = 256;
                        colorGreen = 256;
                        colorBlue = colorCount + 100 - 1883;

                    }

                    if (colorRed > 255) colorRed = 255;
                    if (colorRed < 0) colorRed = 0;
                    if (colorGreen > 255) colorGreen = 255;
                    if (colorGreen < 0) colorGreen = 0;
                    if (colorBlue > 255) colorBlue = 255;
                    if (colorBlue < 0) colorBlue = 0;

                } else {
                    // make the alternating counter-gradient in gray

                    colorRed = (int) (255. - 255. * (((double) colorCountO) / 4077.));
                    colorGreen = colorRed;
                    colorBlue = colorRed;

                }

                fprintf(fp, "%02X%02X%02X\n",
                            colorRed, colorGreen, colorBlue);

            } while (++colorCountO < 4077);

            return 4077;
            break;

        case 9: // prism-likem with yellow in the middle (50%)

            colorCount = 0;
            fprintf(fp, " 512 4\n");

            do {
                // red -> yellow (256 steps)

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%02X%02X%02X\n",
                    255,
                    colorCount,
                    0                        );

            } while (++colorCount < 256);

            do {
                // yellow -> green (128 steps)

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%02X%02X%02X\n",
                    255 - (colorCount-256) * 2,
                    255,
                    0                        );

            } while (++colorCount < 384);

            do {
                // green -> blue (64 steps)

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%02X%02X%02X\n",
                    0,
                    255 - (colorCount - 384) * 4,
                    (colorCount - 384) * 4);

            } while (++colorCount < 448);

            do {
                // blue -> purple (64 steps)

                fprintf(fp, "%04X c ", colorCount);
                fprintf(fp, "#");
                fprintf(fp, "%02X%02X%02X\n",
                    (colorCount - 448) * 4,
                    0,
                    255);

            } while (++colorCount < 512);

            return 512;
            break;

        default: // unknown template

            printf("PANIC - Unknown color template\n\n**-BP5-**\n");
            return -1;

    }

}

// ----------------------------------------------------------------------
// print color template info
// ----------------------------------------------------------------------

void printRecursiveSquaringColorTemplateInfo() {
// prints documentation on the existing color templates

    printf("\nINFO: Color Templates\n");
    printf("   0: 16 shades of gray,\n");
    printf("      from white (divergent) to black (convergent)\n");
    printf("   1: 256 shades of gray\n");
    printf("   2: 766 shades of gray\n");
    printf("   3: color sweep over 2039 colors\n");
    printf("   4: 16 consecutive 766 shades of gray\n");
    printf("      (this artificially introduces 15 hard\n");
    printf("      black-white contrasts throughout the sweep)\n");
    printf("   5: 1 + 256 consecutive 16 shades of gray\n");
    printf("      (alignment of the 256 black-white contrasts\n");
    printf("      is chosen such that white always falls on a\n");
    printf("      1/(2^N) convergence percentage, with N=0..255)\n");
    printf("   6: 1 + 4096 black-white alternating\n");
    printf("      (introduces extreme hard contrast;\n");
    printf("      good for quantitative analysis of 'wspace.csv')\n");
    printf("   7: 1 + 4096 color gradient from light green\n");
    printf("      (divergent) to red (convergent), alternating\n");
    printf("      with static blue for hard contrast;\n");
    printf("      good for quantitative analysis of 'wspace.csv'\n");
    printf("   8: 4077 color sweep that is an overlay from\n");
    printf("      color template '3' and alternating shades\n");
    printf("      of gray; while the colors get lighter to\n");
    printf("      indicate divergence, the shades of gray\n");
    printf("      get darker; combines hard contrast with\n");
    printf("      richness of colors to distinguish plateaus\n");
    printf("   9: 512 color sweep that roughly immitates a light spectrum\n");
    printf("      from red (convergence) to purple (divergence)\n\n");

}

// ----------------------------------------------------------------------
// print iteration counter (after optional increment)
// ----------------------------------------------------------------------

void printRecursionCounter(int addBefore) {
// since in some C implementation the type "unsigned long
// int" is still 32 bits, manually separate the
// iteration / recursion counter into a low- and high-
// counter, with 9 digits each. Perform a manual
// overflow

    if (addBefore == 1) {

        iterationCounterLow = iterationCounterLow + 1;

        if (iterationCounterLow == 1000000000) {
            // overflow

            iterationCounterHigh = iterationCounterHigh + 1;
            iterationCounterLow = 0;
        }

    } else {
    // print

        if (iterationCounterHigh == 0) {
            printf("%lu", iterationCounterLow);
        } else {
            printf("%lu . %09lu x 10e9", iterationCounterHigh, iterationCounterLow);
        }

    }

}
